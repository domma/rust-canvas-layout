use seed::{prelude::*, *};
use crate::groups::Groups;

pub fn main<Msg>(groups: &Groups) -> seed::virtual_dom::Node<Msg> {
    let x : Vec<seed::virtual_dom::Node<Msg>> = groups.iter().enumerate().map(|(i, _)| {
            div![format!("Group {}", i+1)]
        }).collect();

    div![
        "abcasdf",
        x
    ]

}