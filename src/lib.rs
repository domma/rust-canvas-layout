// (Lines like the one below ignore selected Clippy rules
//  - it's useful when you want to check your code with `cargo make verify`
// but some rules are too "annoying" or are not applicable for your case.)
#![allow(clippy::wildcard_imports)]

mod groups;
mod layout;

use groups::{Group, Groups};

use seed::{prelude::*, *};
use web_sys::{HtmlCanvasElement};

// ------ ------
//     Init
// ------ ------

// `init` describes what should happen when your app started.
fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.after_next_render(|_| { Msg::Draw});
    Model{ canvas: ElRef::default(), counter: 0, groups: Group::samples()}
}

// ------ ------
//     Model
// ------ ------

// `Model` describes our app state.
#[derive(Debug)]
struct Model {
    canvas: ElRef<HtmlCanvasElement>,
    counter: i32,
    groups: Groups
}
// ------ ------
//    Update
// ------ ------

// (Remove the line below once any of your `Msg` variants doesn't implement `Copy`.)
#[derive(Copy, Clone)]
// `Msg` describes the different events you can modify state with.
enum Msg {
    Draw,
}

// `update` describes how to handle each `Msg`.
fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        Msg::Draw => {
            log!("Drawing!");
            log!(model.canvas);

            if let Some(canvas) = model.canvas.get() {
                let context = canvas.get_context("2d").unwrap()
                .unwrap()
                .dyn_into::<web_sys::CanvasRenderingContext2d>()
                .unwrap();
                

//                context.stroke_rect(75.0, 140.0, 150.0, 110.0);

                context.set_font("12pt Bitter serif");
                context.set_text_baseline("middle");

                model.groups.iter_mut().for_each(|g| {

                    g.labels.iter_mut().for_each(|l| {
                        l.metrics = Some(context.measure_text(&l.text).unwrap());
                    })
                });

                let mut off_x = 50.0;
                let off_y = 50.0;

                let rect_height = 60.0;

                let border = 20.0;

                model.groups.iter().for_each(|g|{

                    let bounding_box = g.calc_bounding_box();

                    let pos_x = off_x;
                    let mut pos_y = off_y;

                    g.labels.iter().for_each(|l|{

                        context.fill_text(&l.text,
                            pos_x + (border / 2.0) + (bounding_box.width - l.width()) / 2.0
                        
                        , pos_y + (rect_height / 2.0)).unwrap();
                        context.stroke_rect(pos_x, pos_y, bounding_box.width + border, 50.0);

                        pos_y += rect_height + (border / 2.0);
                    });

                    off_x += bounding_box.width + (2.0 * border);
                });

//                context.fill_text("hello world", 30.0, 30.0).unwrap();

//                let x = context.measure_text("bla bla");

//                log!(x);


            } else {
                log!("Could not get HtmlCanvasElement");
            }

        },
    }
}

// ------ ------
//     View
// ------ ------

// (Remove the line below once your `Model` become more complex.)
#[allow(clippy::trivially_copy_pass_by_ref)]
// `view` describes what to display.
fn view(model: &Model) -> Node<Msg> {
    div![
        canvas![
            el_ref(&model.canvas),
            attrs!{At::Width => 800, At::Height => 500}
        ],
//        layout::main(&model.groups),

/*        div![
            "This is a counter: ",
            C!["counter"],
            button!["xx", ev(Ev::Click, |_| Msg::Draw),],
        ]*/
    ]
}

// ------ ------
//     Start
// ------ ------

// (This function is invoked by `init` function in `index.html`.)
#[wasm_bindgen(start)]
pub fn start() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
}
