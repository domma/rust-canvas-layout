use web_sys::{TextMetrics};

#[derive(Debug)]
pub struct BoundingBox {
    pub width: f64,
    pub height: f64
}
#[derive(Debug)]
pub struct Group {
    pub labels: Vec<Label>
}

pub type Groups = Vec<Group>;

impl Group {
    pub fn samples() -> Groups {
        vec![
            Group{labels: vec![
                Label {text: "bla".to_string(), metrics: None}
            ]},
            Group{labels: vec![
                Label {text: "short".to_string(), metrics: None},
                Label {text: "a text that is much longer ... to extend the box".to_string(), metrics: None},
                Label {text: "dummy text".to_string(), metrics: None},
            ]},
            Group{labels: vec![
                Label {text: "another text".to_string(), metrics: None},
                Label {text: "yet another text".to_string(), metrics: None},
            ]},
        ]
    }

    pub fn calc_bounding_box(self: &Group) -> BoundingBox {

        let mut bounding_box = BoundingBox{width: 0.0, height: 0.0};

        self.labels.iter().for_each(|l| {

            if let Some(m) = &l.metrics {

                bounding_box.width = f64::max(bounding_box.width, m.width());
            }
        });

        bounding_box
    }
}


#[derive(Debug)]
pub struct Label {
    pub text: String,
    pub metrics: Option<TextMetrics>
}

impl Label {
    pub fn width(self: &Label) -> f64 {
        match &self.metrics {
            Some(m) => m.width(),
            None => 0.0
        }
    }
}